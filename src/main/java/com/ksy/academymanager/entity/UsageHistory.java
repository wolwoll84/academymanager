package com.ksy.academymanager.entity;

import com.ksy.academymanager.interfaces.CommonModelBuilder;
import com.ksy.academymanager.model.UsageHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipmentId", nullable = false)
    private Equipment equipment;

    @Column(nullable = false)
    private LocalDate dateUsage;

    @Column(nullable = false)
    private Integer timeUsage;

    @Column(nullable = false, length = 30)
    private String nameUsage;

    private UsageHistory(UsageHistoryBuilder builder) {
        this.equipment = builder.equipment;
        this.dateUsage = builder.dateUsage;
        this.timeUsage = builder.timeUsage;
        this.nameUsage = builder.nameUsage;
    }

    public static class UsageHistoryBuilder implements CommonModelBuilder<UsageHistory> {
        private final Equipment equipment;
        private final LocalDate dateUsage;
        private final Integer timeUsage;
        private final String nameUsage;

        public UsageHistoryBuilder(Equipment equipment, UsageHistoryRequest request) {
            this.equipment = equipment;
            this.dateUsage = request.getDateUsage();
            this.timeUsage = request.getTimeUsage();
            this.nameUsage = request.getNameUsage();
        }

        @Override
        public UsageHistory build() {
            return new UsageHistory(this);
        }
    }
}
