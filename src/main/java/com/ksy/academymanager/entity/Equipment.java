package com.ksy.academymanager.entity;

import com.ksy.academymanager.enums.Classroom;
import com.ksy.academymanager.enums.EquipCategory;
import com.ksy.academymanager.enums.Manager;
import com.ksy.academymanager.interfaces.CommonModelBuilder;
import com.ksy.academymanager.model.EquipmentRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 5)
    private Classroom classroom;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private EquipCategory equipCategory;

    @Column(nullable = false, length = 10)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Manager manager;

    @Column(nullable = false)
    private Boolean isDiscard;

    @Column(nullable = false)
    private LocalDate dateReceive;

    private LocalDate dateDisposal;

    private Equipment(equipmentBuilder builder) {
        this.classroom = builder.classroom;
        this.equipCategory = builder.equipCategory;
        this.name = builder.name;
        this.manager = builder.manager;
        this.isDiscard = builder.isDiscard;
        this.dateReceive = builder.dateReceive;
    }

    public static class equipmentBuilder implements CommonModelBuilder<Equipment> {
        private final Classroom classroom;
        private final EquipCategory equipCategory;
        private final String name;
        private final Manager manager;
        private final Boolean isDiscard;
        private final LocalDate dateReceive;

        public equipmentBuilder(EquipmentRequest request) {
            this.classroom = request.getClassroom();
            this.equipCategory = request.getEquipCategory();
            this.name = request.getName();
            this.manager = request.getManager();
            this.isDiscard = false;
            this.dateReceive = LocalDate.now();
        }

        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }
}
