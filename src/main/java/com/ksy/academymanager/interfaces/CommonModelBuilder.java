package com.ksy.academymanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
