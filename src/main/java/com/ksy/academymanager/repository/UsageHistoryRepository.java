package com.ksy.academymanager.repository;

import com.ksy.academymanager.entity.UsageHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;


public interface UsageHistoryRepository extends JpaRepository<UsageHistory, Long> {
    List<UsageHistory> findAllByDateUsageBetweenOrderByIdDesc(LocalDate start, LocalDate end);
}
