package com.ksy.academymanager.repository;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.enums.Classroom;
import com.ksy.academymanager.enums.EquipCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    List<Equipment> findAllByClassroomAndIsDiscardOrderByIdDesc(Classroom classroom, boolean isDiscard);
    List<Equipment> findAllByEquipCategoryOrderByIdDesc(EquipCategory equipCategory);
    List<Equipment> findAllByIsDiscardOrderByIdDesc(boolean isDiscard);
}
