package com.ksy.academymanager.service;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.enums.Classroom;
import com.ksy.academymanager.enums.EquipCategory;
import com.ksy.academymanager.exception.CMissingDataException;
import com.ksy.academymanager.model.*;
import com.ksy.academymanager.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    public void setEquip(EquipmentRequest request) {
        Equipment addData = new Equipment.equipmentBuilder(request).build();

        equipmentRepository.save(addData);
    }

    public Equipment getEquipmentData(long equipmentId) {
        return equipmentRepository.findById(equipmentId).orElseThrow(CMissingDataException::new);
    }

    public ListResult<EquipmentItem> getEquipments() {
        List<EquipmentItem> result = new LinkedList<>();

        List<Equipment> originData = equipmentRepository.findAll();

        originData.forEach(item -> result.add(new EquipmentItem.EquipmentItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ClassroomSearchItem> getClassroomsSearch(Classroom classroom) {
        List<ClassroomSearchItem> result = new LinkedList<>();

        List<Equipment> originData = equipmentRepository.findAllByClassroomAndIsDiscardOrderByIdDesc(classroom, false);

        originData.forEach(item -> result.add(new ClassroomSearchItem.ClassroomSearchItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<CategorySearchItem> getCategoriesSearch(EquipCategory equipCategory) {
        List<CategorySearchItem> result = new LinkedList<>();

        List<Equipment> originData = equipmentRepository.findAllByEquipCategoryOrderByIdDesc(equipCategory);

        originData.forEach(item -> result.add(new CategorySearchItem.CategorySearchItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ClassroomSearchItem> getDiscards() {
        List<ClassroomSearchItem> result = new LinkedList<>();

        List<Equipment> originData = equipmentRepository.findAllByIsDiscardOrderByIdDesc(true);

        originData.forEach(item -> result.add(new ClassroomSearchItem.ClassroomSearchItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public EquipmentResponse getEquipmentDetail(long id) {
        Equipment originData = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new EquipmentResponse.EquipmentResponseBuilder(originData).build();
    }
}

