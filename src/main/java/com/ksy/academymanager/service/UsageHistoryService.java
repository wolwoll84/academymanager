package com.ksy.academymanager.service;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.entity.UsageHistory;
import com.ksy.academymanager.exception.CMissingDataException;
import com.ksy.academymanager.model.*;
import com.ksy.academymanager.repository.UsageHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageHistoryService {
    private final UsageHistoryRepository usageHistoryRepository;

    public void setUsageHistory(Equipment equipment, UsageHistoryRequest request) {
        UsageHistory addData = new UsageHistory.UsageHistoryBuilder(equipment, request).build();

        usageHistoryRepository.save(addData);
    }

    public ListResult<UsageHistoryItem> getHistories() {
        List<UsageHistoryItem> result = new LinkedList<>();

        List<UsageHistory> originData = usageHistoryRepository.findAll();

        originData.forEach(item -> result.add(new UsageHistoryItem.UsageHistoryItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<HistoryByMonthItem> getHistoryByMonth(LocalDate start, LocalDate end) {
        List<HistoryByMonthItem> result = new LinkedList<>();

        List<UsageHistory> originData = usageHistoryRepository.findAllByDateUsageBetweenOrderByIdDesc(start, end);

        originData.forEach(item -> result.add(new HistoryByMonthItem.HistoryByMonthItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public UsageHistoryResponse getHistoryDetail(long id) {
        UsageHistory originData = usageHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new UsageHistoryResponse.UsageHistoryResponseBuilder(originData).build();
    }
}
