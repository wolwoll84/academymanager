package com.ksy.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Classroom {
    ONE("1강의실"),
    TWO("2강의실"),
    THREE("3강의실")
    ;
    private final String name;
}
