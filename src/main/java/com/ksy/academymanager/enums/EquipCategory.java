package com.ksy.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EquipCategory {
    MONITOR("모니터"),
    KEYBOARD("키보드"),
    MOUSE("마우스"),
    DESKTOP("데스크탑"),
    STATIONERY("사무용품"),
    AIR_CONDITIONER("에어컨"),
    HEATER("히터")
    ;

    private final String name;
}
