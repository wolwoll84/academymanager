package com.ksy.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Manager {
    KIM_NANA("김나나"),
    GA_NADA("가나다"),
    SO_NAMU("소나무")
    ;

    private final String name;
}
