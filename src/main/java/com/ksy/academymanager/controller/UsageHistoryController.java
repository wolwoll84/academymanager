package com.ksy.academymanager.controller;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.model.*;
import com.ksy.academymanager.service.EquipmentService;
import com.ksy.academymanager.service.ResponseService;
import com.ksy.academymanager.service.UsageHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class UsageHistoryController {
    private final UsageHistoryService usageHistoryService;
    private final EquipmentService equipmentService;

    @PostMapping("/new/equipment-id/{equipmentId}")
    public CommonResult setUsageHistory(@PathVariable long equipmentId, @RequestBody @Valid UsageHistoryRequest request) {
        Equipment equipment = equipmentService.getEquipmentData(equipmentId);

        usageHistoryService.setUsageHistory(equipment, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<UsageHistoryItem> getHistories() {
        return ResponseService.getListResult(usageHistoryService.getHistories(), true);
    }

    @GetMapping("/all/month")
    public ListResult<HistoryByMonthItem> getHistoryByMonth(
            @RequestParam("start") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
            @RequestParam("end") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end
    ) {
        return ResponseService.getListResult(usageHistoryService.getHistoryByMonth(start, end), true);
    }

    @GetMapping("/detail/{id}")
    public SingleResult<UsageHistoryResponse> getHistory(@PathVariable long id) {
        return ResponseService.getSingleResult(usageHistoryService.getHistoryDetail(id));
    }
}
