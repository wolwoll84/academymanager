package com.ksy.academymanager.controller;

import com.ksy.academymanager.enums.Classroom;
import com.ksy.academymanager.enums.EquipCategory;
import com.ksy.academymanager.model.*;
import com.ksy.academymanager.service.EquipmentService;
import com.ksy.academymanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/equip")
public class EquipmentController {
    private final EquipmentService equipmentService;

    @PostMapping("/new")
    public CommonResult setEquip(@RequestBody @Valid EquipmentRequest request) {
        equipmentService.setEquip(request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<EquipmentItem> getEquipments() {
        return ResponseService.getListResult(equipmentService.getEquipments(), true);
    }

    @GetMapping("/all/classroom")
    public ListResult<ClassroomSearchItem> getClassroomsSearch(@RequestParam("classroom") Classroom classroom) {
        return ResponseService.getListResult(equipmentService.getClassroomsSearch(classroom), true);
    }

    @GetMapping("/all/category")
    public ListResult<CategorySearchItem> getCategoriesSearch(@RequestParam("category") EquipCategory category) {
        return ResponseService.getListResult(equipmentService.getCategoriesSearch(category), true);
    }

    @GetMapping("/all/discard")
    public ListResult<ClassroomSearchItem> getDiscards() {
        return ResponseService.getListResult(equipmentService.getDiscards(), true);
    }

    @GetMapping("/detail/{id}")
    public SingleResult<EquipmentResponse> getEquipmentDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(equipmentService.getEquipmentDetail(id));
    }
}
