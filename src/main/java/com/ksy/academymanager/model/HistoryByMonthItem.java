package com.ksy.academymanager.model;

import com.ksy.academymanager.entity.UsageHistory;
import com.ksy.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryByMonthItem {
    private Long id;
    private Long equipmentId;
    private LocalDate dateUsage;
    private Integer timeUsage;
    private String nameUsage;

    private HistoryByMonthItem(HistoryByMonthItemBuilder builder) {
        this.id = builder.id;
        this.equipmentId = builder.equipmentId;
        this.dateUsage = builder.dateUsage;
        this.timeUsage = builder.timeUsage;
        this.nameUsage = builder.nameUsage;
    }

    public static class HistoryByMonthItemBuilder implements CommonModelBuilder<HistoryByMonthItem> {
        private final Long id;
        private final Long equipmentId;
        private final LocalDate dateUsage;
        private final Integer timeUsage;
        private final String nameUsage;

        public HistoryByMonthItemBuilder(UsageHistory usageHistory) {
            this.id = usageHistory.getId();
            this.equipmentId = usageHistory.getEquipment().getId();
            this.dateUsage = usageHistory.getDateUsage();
            this.timeUsage = usageHistory.getTimeUsage();
            this.nameUsage = usageHistory.getNameUsage();
        }

        @Override
        public HistoryByMonthItem build() {
            return new HistoryByMonthItem(this);
        }
    }
}
