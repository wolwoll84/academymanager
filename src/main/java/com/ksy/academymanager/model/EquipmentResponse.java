package com.ksy.academymanager.model;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentResponse {
    private Long id;
    private String classroomName;
    private String equipCategoryName;
    private String name;
    private String managerName;
    private String isDiscardName;
    private LocalDate dateReceive;
    private LocalDate dateDisposal;

    private EquipmentResponse(EquipmentResponseBuilder builder) {
        this.id = builder.id;
        this.classroomName = builder.classroomName;
        this.equipCategoryName = builder.equipCategoryName;
        this.name = builder.name;
        this.managerName = builder.managerName;
        this.isDiscardName = builder.isDiscardName;
        this.dateReceive = builder.dateReceive;
        this.dateDisposal = builder.dateDisposal;
    }

    public static class EquipmentResponseBuilder implements CommonModelBuilder<EquipmentResponse> {
        private final Long id;
        private final String classroomName;
        private final String equipCategoryName;
        private final String name;
        private final String managerName;
        private final String isDiscardName;
        private final LocalDate dateReceive;
        private LocalDate dateDisposal;

        public EquipmentResponseBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.classroomName = equipment.getClassroom().getName();
            this.equipCategoryName = equipment.getEquipCategory().getName();
            this.name = equipment.getName();
            this.managerName = equipment.getManager().getName();
            this.isDiscardName = equipment.getIsDiscard() ? "폐기" : "정상";
            this.dateReceive = equipment.getDateReceive();
            this.dateDisposal = equipment.getDateDisposal();
        }

        @Override
        public EquipmentResponse build() {
            return new EquipmentResponse(this);
        }
    }
}
