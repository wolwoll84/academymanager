package com.ksy.academymanager.model;

import com.ksy.academymanager.entity.UsageHistory;
import com.ksy.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistoryItem {
    private Long historyId;
    private String historyName;
    private String equipmentName;
    private LocalDate dateUsage;

    private UsageHistoryItem(UsageHistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.historyName = builder.historyName;
        this.equipmentName = builder.equipmentName;
        this.dateUsage = builder.dateUsage;
    }

    public static class UsageHistoryItemBuilder implements CommonModelBuilder<UsageHistoryItem> {
        private final Long historyId;
        private final String historyName;
        private final String equipmentName;
        private final LocalDate dateUsage;

        public UsageHistoryItemBuilder(UsageHistory usageHistory) {
            this.historyId = usageHistory.getId();
            this.historyName = usageHistory.getNameUsage();
            this.equipmentName = usageHistory.getEquipment().getEquipCategory() + " / " + usageHistory.getEquipment().getName();
            this.dateUsage = usageHistory.getDateUsage();
        }

        @Override
        public UsageHistoryItem build() {
            return new UsageHistoryItem(this);
        }
    }
}
