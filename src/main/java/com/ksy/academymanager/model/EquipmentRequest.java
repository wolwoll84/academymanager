package com.ksy.academymanager.model;

import com.ksy.academymanager.enums.Classroom;
import com.ksy.academymanager.enums.EquipCategory;
import com.ksy.academymanager.enums.Manager;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EquipmentRequest {
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private Classroom classroom;

    @Enumerated(value = EnumType.STRING)
    @NotNull
    private EquipCategory equipCategory;

    @NotNull
    @Length(min = 1, max = 10)
    private String name;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Manager manager;
}
