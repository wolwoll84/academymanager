package com.ksy.academymanager.model;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.entity.UsageHistory;
import com.ksy.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistoryResponse {
    private Long historyId;
    private Long equipmentId;
    private LocalDate dateUsage;
    private Integer timeUsage;
    private String nameUsage;

    private UsageHistoryResponse(UsageHistoryResponseBuilder builder) {
        this.historyId = builder.historyId;
        this.equipmentId = builder.equipmentId;
        this.dateUsage = builder.dateUsage;
        this.timeUsage = builder.timeUsage;
        this.nameUsage = builder.nameUsage;
    }

    public static class UsageHistoryResponseBuilder implements CommonModelBuilder<UsageHistoryResponse> {
        private final Long historyId;
        private final Long equipmentId;
        private final LocalDate dateUsage;
        private final Integer timeUsage;
        private final String nameUsage;

        public UsageHistoryResponseBuilder(UsageHistory usageHistory) {
            this.historyId = usageHistory.getId();
            this.equipmentId = usageHistory.getEquipment().getId();
            this.dateUsage = usageHistory.getDateUsage();
            this.timeUsage = usageHistory.getTimeUsage();
            this.nameUsage = usageHistory.getNameUsage();
        }

        @Override
        public UsageHistoryResponse build() {
            return new UsageHistoryResponse(this);
        }
    }
}
