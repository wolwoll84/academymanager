package com.ksy.academymanager.model;

import com.ksy.academymanager.entity.Equipment;
import com.ksy.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {
    private Long id;
    private String classroomName;
    private String equipCategoryName;
    private String name;

    private EquipmentItem(EquipmentItemBuilder builder) {
        this.id = builder.id;
        this.classroomName = builder.classroomName;
        this.equipCategoryName = builder.equipCategoryName;
        this.name = builder.name;
    }

    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem> {
        private final Long id;
        private final String classroomName;
        private final String equipCategoryName;
        private final String name;

        public EquipmentItemBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.classroomName = equipment.getClassroom().getName();
            this.equipCategoryName = equipment.getEquipCategory().getName();
            this.name = equipment.getName();
        }

        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }
}
