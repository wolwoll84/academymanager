package com.ksy.academymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class UsageHistoryRequest {
    @NotNull
    private LocalDate dateUsage;

    @NotNull
    private Integer timeUsage;

    @NotNull
    @Length(min = 1, max = 30)
    private String nameUsage;
}
